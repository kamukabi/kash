#!/bin/bash

INSTALL_TO=""
OS=$(uname)
NAME="kash"

echo;
echo "Installing $NAME version $(cat ./VERSION)";

mkdir -p "$HOME/.kk/bin"
cp ./$NAME "$HOME/.kk/bin"

case "$SHELL" in
    /bin/bash | /usr/bin/bash )
        case $OS in
          'Linux')
            INSTALL_TO="$HOME/.bashrc"
            ;;
          'Darwin')
            INSTALL_TO="$HOME/.bash_profile"
            ;;
          *) ;;
        esac
        ;;
    /bin/zsh | /usr/bin/zsh )
        INSTALL_TO="$HOME/.zshrc"
        ;;
    * )
        echo "Sorry, your shell is not supported"
        echo "You can open issue on https://bitbucket.org/kamukabi/$NAME/issues"
        echo "Please feel free to implement it and make pull requests"
        exit 1
        ;;
esac

if [ ! -f "$INSTALL_TO" ] ; then
    echo "I wanted to add $HOME/.kk/bin to your path, but $INSTALL_TO file not exists"
    echo "Plese open issue on https://bitbucket.org/kamukabi/$NAME/issues"
    exit 0
fi

if ! grep -q "PATH=\$PATH:$HOME/.kk/bin" "$INSTALL_TO"; then
    echo "export PATH=\$PATH:$HOME/.kk/bin" >> "$INSTALL_TO"
fi

echo "$NAME instalation completed"

# completion
case "$SHELL" in
    /bin/bash | /usr/bin/bash )
        if [ -d /etc/bash_completion.d ]; then
            REPLY=""
            read -p "Do you want to install bash completition? This action require sudo [y/N] " -n 1 -r
            if [ -n "$REPLY" ]; then
                echo
                if [  $REPLY == "y" -o $REPLY == "Y" ]; then
                    sudo cp -v bash_completion/$NAME /etc/bash_completion.d/
                    echo "bash completition will be enabled when you restart your shell."
                fi
            fi
        else
            echo "If you want bash completition you need to install bash_completion package first."
        fi
        ;;
    /bin/zsh | /usr/bin/zsh )
# TODO
        ;;
esac
