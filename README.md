Kash - Kamukabi Commandline Toolkit
===============

Good day sir!

The following toolbox is specially designed to interact with Kamukabi hosting. We hope it will meet your expectations.
In case of any problems, please create issue or contact us by email:  tech AT kamukabi.com

## Dependencies

Those binaries should be available in user PATH:

* ssh
* sft
* git

## Installation

To install our toolbox you just need to run intall.sh from this repo. Please
read it before you do it. Never run someone's script before you read them. You can also use this script to update your kash version (after pulling new code of course).

```sh
user@linux:/kash/ $ ./install.sh
user@linux:~ $ kash
Kamukabi toolbox 0.2.0
Usage:
  app:logs     {-f|NUM}   - show app output
  app:rebuild            - restart build process
  app:restart            - restart your application
  app:run                - send command to application
  clone                  - clone application repository
  deploy                 - send code to kamukabi server
  ftp                    - start ftp shell on your app storage
  init                   - initialize git repo in current directory and set remotes
  pg:backup              - backup your database to ftp
  pg:cli                 - open psql cli
  pg:restore             - restore your database from database.sql on ftp

```

Bash completion can be installed from bash_completion folder:

```sh
user@linux:~ $ sudo cp bash_completion/kash /etc/bash_completion.d/
user@linux:~ $ kash [Tab][Tab]
app:logs     app:rebuild  app:restart  app:run      app:stdout   clone        deploy       ftp          init         pg:backup    pg:restore   pg:sql

```

## Usage examples

Let us assume that your user and ssh key was added to application *myproject* on kamukabi.

### Creating rails sample project

```sh
user@linux:~ $ rails new myproject/
user@linux:~ $ cd myproject
user@linux:~/myproject/ $ kash init
user@linux:~/myproject/ $ bundle install
user@linux:~/myproject/ $ kash deploy

```

### Debug application problem

If your deployment went without problems but under your domain you see only 'there is nothing here' message or your application is not responding you can sneak peek application output.

```sh
user@linux:~/myproject/ $ kash app:logs
rails error: somthing wrong

```

If you need to run additional command, you can do it with kash app:run and later restart app with kash app:restart.

### Restore database

Let us assume that we have database schema in abc.sql. We can restore it in two ways: by pasting it to postgresql console or sending it to ftp and using kash to restore it.

```sh
user@linux:~/myproject/ $ kash pg:sql < abc.sql

```

```sh
user@linux:~/myproject/ $ kash ftp
sftp> put abc.sql database.sql
user@linux:~/myproject/ $ kash pg:restore

```

### Backup database

When you request database backup with kash, your dump will be placed on your ftp root dir named as database.sql. You can download it later by ftp client.

```sh
user@linux:~/myproject/ $ kash pg:backup
user@linux:~/myproject/ $ kash ftp
sftp> get database.sql
user@linux:~/myproject/ $ ls database.sql
database.sql

```
